# 100 Days Of Code - Log

### Day 8: February 28, 2017

#### Today's Progress

* Changed from a for-each to a for-in
* Trying to push items to new arrays with their new respective values
* ... but data doesn't get displayed or logged

**Thoughts:**
Of course since there are values within the arrays that I need to get out and put together again, I switched around a bit. But as usual I start coding too late so my mind easily wanders off. Search value is sent but returns empty objects. Exciting.

**Link to work:**

* [Codepen](http://codepen.io/nefalia/pen/jBNGrg)
* [Commits](https://bitbucket.org/nefalia/wikipedia-viewer/commits/all)

### Day 7: February 27, 2017

#### Today's Progress

* Still experimenting with how to get the values out of the array

**Thoughts:**
Still prints, still doesn't look pretty. I did some experimenting but I can't seem to figure out how to solve this.

**Link to work:**

* [Codepen](http://codepen.io/nefalia/pen/jBNGrg)
* [Commits](https://bitbucket.org/nefalia/wikipedia-viewer/commits/all)

### Day 6: February 26, 2017

#### Today's Progress

* Added callback
* Appending search results into the DOM

**Thoughts:**
It honestly took a few hours until I could wrap my head around printing the search results. Now it prints, but it doesn't look pretty. Next step is to pick and pair the values from the different arrays.

**Link to work:**

* [Codepen](http://codepen.io/nefalia/pen/jBNGrg)
* [Commits](https://bitbucket.org/nefalia/wikipedia-viewer/commits/all)

### Day 5: February 25, 2017

#### Today's Progress

* Moved the function back as it was
* Sending the user input, getting back correct info

**Thoughts:**
I started way too late today so I didn't get the flow today either. But looking at more code to understand exactly wtf I am supposed to do was succesful - user input is sent but not printed out on the page. Oh well. Tomorrow is a new adventure.

**Link to work:**

* [Commits](https://bitbucket.org/nefalia/wikipedia-viewer/commits/all)

### Day 4: February 24, 2017

#### Today's Progress

* Picking up the user input value to connect with the search
* Separate function for the search button

**Thoughts:**
Was a bit distracted today, couldn't relax enough to get in to a decent flow. Let's try again tomorrow.

**Link to work:**

* [Codepen](http://codepen.io/nefalia/pen/jBNGrg)
* [Commits](https://bitbucket.org/nefalia/wikipedia-viewer/commits/all)

### Day 3: February 23, 2017

#### Today's Progress

* Added jQuery
* On clicking Search button, Search button and Random page link disappears and reveals a Reset button
* Exploring the API parameters of Wikipedia
* Finding the right url to access Wikipedia API with JSON

**Thoughts:**
I remember doing this a long time ago while in school. I remember this was much harder than it is now, does that mean that it is easier or have I gotten smarter? Maybe a bit of both.
Still haven't managed to get the data to write itself out, but I guess I am getting there.

**Link to work:**

* [Codepen](http://codepen.io/nefalia/pen/jBNGrg)
* [Commits](https://bitbucket.org/nefalia/wikipedia-viewer/commits/all)

### Day 2: February 22, 2017

#### Today's Progress

* Put up basic elements for index.html
* Put up basic CSS for above elements
* Experimented a bit with showing and hiding the search result element with classes
* Added link to random page on Wikipedia

**Thoughts:**
I will have to be more specific and thought through in the breaking down of this project. I find myself being scattered, doing a bit of this and that all over the place whenever I think of it. Not sure if it is because I am coding from home with distractions or because I'm bad a structuring. Maybe both?

It almost seems too simple to just have a direct link to a random page on Wikipedia. I added a class at least so that it can be hidden when the search result is showing.

I will focus on getting the machinery working, then continue with the looks.

**Link to work:**

* [Codepen](http://codepen.io/nefalia/pen/jBNGrg)
* [Commits](https://bitbucket.org/nefalia/wikipedia-viewer/commits/all)

### Day 1: February 21, 2017

#### Today's Progress

* Cloned repository, edited files accordingly.
* Picked and started project: [Build a Wikipedia viewer](https://www.freecodecamp.com/challenges/build-a-wikipedia-viewer).
* Created basic files and directories.

**Thoughts:**

I want to focus on becoming better at Javascript and understanding API:s. Immediately I started thinking that I wanted to do a wikiviewer with focus on women in technology but then I thought: "baby steps, Maria. Let's start with the basics."
And another thing: please do not code while having a fever late at night. I set it all up, had it all planned out and realized that it's best to totally separate the project from the log files.

*Det man inte har i huvudet får man ha i fötterna.*

**Link to work:**

* [Wikipedia viewer](https://bitbucket.org/nefalia/wikipedia-viewer)
